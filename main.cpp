#include "TVar.h"
#include "Transaction.h"

#include <vector>
#include <string>
#include <thread>
#include <iostream>

// increment once for each action ("eating" for every single philosopher)
//  to approximate how much cpu time each thread gets
std::vector<unsigned> counter(5);

// prints contents of 'counter' to console often or only a few times, depending on parameter 'fast'
void counterPrinter(const std::vector<unsigned> & counter, const bool fast)
{
	while (true)
	{
		if (!fast)
		{
			// sleep for short time to avoid filling console with too many redundant lines
			std::this_thread::sleep_for(std::chrono::microseconds(1));
		}

		std::string counter_string;
		for (unsigned c : counter)
		{
			counter_string.append(std::to_string(c) + " ");
		}
		std::cout << "action counter: " + counter_string + "\n";
	}
}


//put_stick(S)->write_tvar(S, true).

void put_stick(Transaction<bool> & t, TVar<bool> & stick)
{
	t.WriteTVar(stick, true);
}

//take_stick(S) ->
//	case read_tvar(S) of
//		true->write_tvar(S, false);
//		false->retry()
//	end.

void take_stick(Transaction<bool> & t, TVar<bool> & stick)
{
	if (t.ReadTVar(stick))
		t.WriteTVar(stick, false);
	else
		t.Retry();
}

//phil(N, SL, SR) ->
//base:printLn(base : show(N)++ " is thinking"),
//	%atomically(fun()->take_stick(SL) end),
//	%atomically(fun()->take_stick(SR) end),
//	atomically(fun()->take_stick(SL),
//		take_stick(SR) end),
//	base : printLn(base : show(N)++ " is eating"),
//	atomically(fun()->put_stick(SL) end),
//	atomically(fun()->put_stick(SR) end),
//	phil(N, SL, SR).

void philosopher(const size_t id, TVar<bool> & left, TVar<bool> & right)
{
	while (true)
	{
		std::cout << "Philosopher " + std::to_string(id) + " thinking... \n";

		// take both sticks at same time
		Atomically<bool>(
			[&](Transaction<bool> & t)
			{
				take_stick(t, left);
				take_stick(t, right); 
			}
		);

		// take sticks separately
		// -> leads to 'deadlock' without crash, can be observed by printing counter
		/*Atomically<bool>(
			[&](Transaction<bool> & t)
			{
				take_stick(t, left);
			}
		);
		Atomically<bool>(
			[&](Transaction<bool> & t)
			{
				take_stick(t, right);
			}
		);*/

		std::cout << "Philosopher " + std::to_string(id) + " eating... \n";

		// increment counter for testing purposes
		counter[id - 1]++;

		// put both sticks back at the same time
		Atomically<bool>(
			[&](Transaction<bool> & t)
			{
				put_stick(t, left);
				put_stick(t, right);
			}
		);
	}
}

//start()->
//	S1 = atomically(fun()->new_stick() end),
//	S2 = atomically(fun()->new_stick() end),
//	S3 = atomically(fun()->new_stick() end),
//	S4 = atomically(fun()->new_stick() end),
//	S5 = atomically(fun()->new_stick() end),
//	spawn(fun()->phil(1, S1, S2) end),
//	spawn(fun()->phil(2, S2, S3) end),
//	spawn(fun()->phil(3, S3, S4) end),
//	spawn(fun()->phil(4, S4, S5) end),
//	%base:getLine(),
//	phil(5, S5, S1).

void fivePhilosophers()
{
	TVar<bool> s1(true);
	TVar<bool> s2(true);
	TVar<bool> s3(true);
	TVar<bool> s4(true);
	TVar<bool> s5(true);

	std::thread p1(philosopher, 1, std::ref(s1), std::ref(s2));
	std::thread p2(philosopher, 2, std::ref(s2), std::ref(s3));
	std::thread p3(philosopher, 3, std::ref(s3), std::ref(s4));
	std::thread p4(philosopher, 4, std::ref(s4), std::ref(s5));
	
	std::cout << "press enter to start 5th philospher \n";
	std::cin.get();
	std::thread p5(philosopher, 5, std::ref(s5), std::ref(s1));
	
	// print in main thread the counter for testing purposes
	counterPrinter(counter, true);
}

void twoPhilosophers()
{
	TVar<bool> s1(true);
	TVar<bool> s2(true);

	std::thread p1(philosopher, 1, std::ref(s1), std::ref(s2));

	std::cout << "press enter to start 2nd philospher \n";
	std::cin.get();
	std::thread p2(philosopher, 2, std::ref(s2), std::ref(s1));
	
	// print in main thread the counter for testing purposes
	counterPrinter(counter, true);
}

// change can be positive or negative
void changeBalance(Transaction<int> & t, TVar<int> & account, const int change)
{
	t.WriteTVar(account, t.ReadTVar(account) + change);
}

void continuousTransfer(const size_t id, TVar<int> & source, TVar<int> & target)
{
	// only do fixed number of transfers
	while (counter[id - 1] < 5000)
	{
		// make sure that the sum of the two account balances stays the same
		/*std::cout << "transfer with id: " + std::to_string(id) +
		", source balance: " << source.read().first <<
		" and target balance: " << target.read().first << "\n";*/

		Atomically<int>(
			[&](Transaction<int> & t)
			{
				
				changeBalance(t, source, -1);
				changeBalance(t, target, +1);
			}
		);

		// increment counter for testing purposes
		counter[id - 1]++;
		
		
	}
	
	
}

void onlineBanking()
{
	// account initialized with starting balance
	TVar<int> a1(50);
	TVar<int> a2(50);

	std::thread t1(continuousTransfer, 1, std::ref(a1), std::ref(a2));

	std::cout << "press enter to start 2nd transfer thread \n";
	std::cin.get();

	std::thread t2(continuousTransfer, 2, std::ref(a2), std::ref(a1));
	//continuousTransfer(2, a2, a1);

	t1.join();
	t2.join();
	std::cout << "finished fixed transfers \n";
	std::cout << "balance1: " << a1.read().first << " balance2: " << a2.read().first << "\n";
	std::cout << "press enter to close \n";
	std::cin.get();
	

	// print in main thread the counter for testing purposes
	//counterPrinter(counter, false);
}

int main(int argc, char *argv[])
{
	std::cout << "starting... \n";
	// with command line parameters
	if (argc > 1)
	{
		if (std::string("-phil2").compare(argv[1]) == 0)
		{
			twoPhilosophers();
		}
		else if (std::string("-phil5").compare(argv[1]) == 0)
		{
			fivePhilosophers();
		}
		else if(std::string("-bank").compare(argv[1]) == 0)
		{
			onlineBanking();
		}
		else
		{
			std::cout << "invalid command line parameters \n";
			std::cout << "press enter to close \n";
			std::cin.get();
		}
	}
	else
	{
		std::cout << "no command line parameters \n";
		std::cout << "press enter to close \n";
		std::cin.get();
	}
	

	//twoPhilosophers();

	//fivePhilosophers();

	//onlineBanking();

}
