#pragma once

#include <utility> // std::pair
#include <iostream>

#include <mutex>
#include <thread>



template<typename T>
class TVar
{
private:
	std::mutex mutex_; 
	T value_; // balance, stick bool, etc
	unsigned version_; // starts at 0
	
public:
	// constructor initializes unique_lock without locking the associated mutex
	TVar(const T value) :
		value_(value), version_(0) {};
	~TVar() {};

	// returns value AND version
	std::pair<T, unsigned> read()
	{
		//std::lock_guard<std::mutex> lock_guard(mutex_);
		return std::pair<T, unsigned>(value_, version_);
	}

	// http://www.cplusplus.com/reference/mutex/unique_lock/lock/
	// http://en.cppreference.com/w/cpp/thread/unique_lock/lock
	void lock()
	{
		mutex_.lock();
	};

	// http://www.cplusplus.com/reference/mutex/unique_lock/unlock/
	// http://en.cppreference.com/w/cpp/thread/unique_lock/unlock
	void unlock()
	{
		mutex_.unlock();
		
	};

	// should normally be safe to write if called in Transaction's "Atomically"-function
	void write(T value)
	{
		value_ = value;
		version_++;
	};
};

// string conversion for printing to console
template<typename T>
std::ostream& operator<<(std::ostream& os, TVar<T>& t_var)
{
	std::pair<T, unsigned> result = t_var.read();
	return os << "value: " << result.first << " version: " + std::to_string(result.second);
}

// comparison operator for 2 Tvars
template<typename T>
inline bool operator<(const TVar<T>& l, const TVar<T>& r)
{
	return (l.version_ < r.version_) && (l.value_ < r.value_);
}


