possible command line parameters:

1) "-phil5" -> 5 philosopher threads
   - Threads 1-4 startet automatically, 5th thread startet when enter-key pressed
   - runs indefinetely
   - action-counter console output can be observed during runtime or after abort
     to observe which philosophers got to eat how many times

2) "-phil2" -> 2 philosopher threads
   - 1st thread startet automatically, 2nd thread startet when enter-key pressed
   - rest same as "-phil5"

3) "-bank" -> 2 banktransfer threads
   - 1st thread startet automatically, 2nd thread startet when enter-key pressed.
   - both accounts start with a balance of 50
   - each account transfers to the other 1 of its currency 5000 times
   - after all transfers are done, the end result should show that both accounts
     again have a balance of 50 without creating or deleting currency during the process