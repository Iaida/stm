-module(stm).
-export([atomically/1,
         new_tvar/1, read_tvar/1,write_tvar/2,retry/0,
         test/0]).
-import(gb_trees,[empty/0,lookup/2,insert/3,enter/3,to_list/1,
                  keys/1]).

new_tvar(V) -> spawn(fun() -> tvar(V,0,[]) end).

tvar(V,Ver,Susps) ->
  %base:printLn("*"++base:show(self())++","++base:show(V)++"*"),
  receive
    {core_read,P} -> P!{value,V,Ver},
                     tvar(V,Ver,Susps);
    {lock,P}      -> P!locked,
                     tvar_locked(V,Ver,Susps);
    {susp,P}      -> tvar(V,Ver,[P|Susps])
  end.

tvar_locked(V,Ver,Susps) ->
  %base:printLn("<"++base:show(self())++","++base:show(V)++">"),
  receive
    unlock          -> tvar(V,Ver,Susps);
    {core_read,P}   -> P!{value,V,Ver},
                       tvar_locked(V,Ver,Susps);
    {core_write,V1} -> lists:map(fun(P) -> P!continue end, Susps),
                       tvar_locked(V1,Ver+1,[]);
    {unsusp,P}      -> tvar_locked(V,Ver,Susps--[P])
  end.

core_read(Tvar) -> Tvar ! {core_read,self()},
                   receive
                     {value,V,Ver} -> {V,Ver}
                   end.
core_write(Tvar,V) -> Tvar!{core_write,V}.

lock(Tvar) -> Tvar ! {lock,self()},
              receive
                locked -> ok
              end.

unlock(Tvar) -> Tvar ! unlock.

susp(Tvar,Filter) -> Tvar!{susp,Filter}.

unsusp(Tvar,Filter) -> Tvar!{unsusp,Filter}.

read_tvar(Tvar) ->
  %base:printLn({read_tvar,Tvar}),
  WS = get(ws),
  case lookup(Tvar,WS) of
    none      -> {V,Ver} = core_read(Tvar),
                 RS = get(rs),
                 case lookup(Tvar,RS) of
                   none        -> put(rs,insert(Tvar,Ver,RS));
                   {value,Ver} -> ok;
                   {value,_  } -> throw(stm_exeption_rollback)
                 end,
                 V;
    {value,V} -> V
  end.

write_tvar(Tvar,V) ->
  %base:printLn({write_tvar,Tvar}),
  WS = get(ws),
  put(ws,enter(Tvar,V,WS)).

retry() -> throw(stm_exception_retry).

atomically(Trans) ->
  put(ws,empty()),
  put(rs,empty()),
  Me = self(),
  Filter = spawn(fun() -> receive
                            continue -> Me ! continue;
                            stop     -> ok
                          end
                 end),  
  case catch Trans() of
    stm_exception_rollback -> Filter ! stop,
                              atomically(Trans);
    stm_exception_retry ->
      RS = get(rs),
      RL = to_list(RS),
      lists:map(fun({Tvar,_}) -> lock(Tvar) end,RL), 
      %base:printLn(validate_tvars),
      case validate(RL) of
        false -> % invalid => rollback
                 base:printLn(direkt_rollback_in_retry),
                 lists:map(fun({Tvar,_}) -> unlock(Tvar) end,RL),
                 Filter ! stop, 
                 atomically(Trans);
        true  -> % valid => susp
                 %base:printLn(susp),
                 lists:map(fun({Tvar,_}) -> susp(Tvar,Filter) end,RL), 
                 lists:map(fun({Tvar,_})-> unlock(Tvar) end,RL),
                 receive
                   continue -> 
                     lists:map(fun({Tvar,_}) -> unsusp(Tvar,Filter) end,
                               RL),
                     atomically(Trans)
                 end
      end;
    Res      -> Filter ! stop, 
                RS = get(rs),
                RL = to_list(RS),
                WS = get(ws),
                WL = to_list(WS),
                RWL = lists:umerge(keys(RS),keys(WS)),
                %base:printLn({lock_tvars,RWL}),
                lists:map(fun(Tvar) -> lock(Tvar) end,RWL), 
                %base:printLn(validate_tvars),
                case validate(RL) of
                  false -> % invalid => rollback
                           base:printLn(rollback),
                           lists:map(fun(Tvar) -> unlock(Tvar)
                                     end,RWL), 
                           atomically(Trans);
                  true  -> % valid => commit
                           %base:printLn(commit),
                           lists:map(fun({Tvar,V}) -> core_write(Tvar,V)
                                     end,WL), 
                           lists:map(fun(Tvar)-> unlock(Tvar)
                                     end,RWL), 
                           Res
                end
  end.

validate([]) -> true;
validate([{Tvar,Ver}|RL]) ->
  case core_read(Tvar) of
    {_,Ver} -> validate(RL);
    _       -> false
  end.


test() -> T1 = atomically(fun() -> new_tvar(1) end),
          T2 = atomically(fun() -> new_tvar(2) end),
          N = 100,
          Me = self(),
          spawn(fun() -> test1(T1,T2,N,Me) end),
          test2(T1,T2,N),
          receive
            ready -> ok
          end,
          base:printLn("Ausgabe:"),
          V1 = atomically(fun() -> read_tvar(T1) end),
          V2 = atomically(fun() -> read_tvar(T2) end),
          base:printLn({V1,V2}).

test1(_,_,0,P) -> P!ready;
test1(T1,T2,N,P) -> atomically(fun() -> V1 = read_tvar(T1),
                                        write_tvar(T1,V1+1)
                               end),
                    test1(T1,T2,N-1,P).

test2(_,_,0) -> ok;
test2(T1,T2,N) -> atomically(fun() -> V1 = read_tvar(T1),
                                      V2 = read_tvar(T2),
                                      write_tvar(T1,V1+V2)
                             end),
                  test2(T1,T2,N-1).
