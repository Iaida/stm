-module(phil_stm).
-export([start/0]).
-import(stm,[atomically/1,
             new_tvar/1, read_tvar/1,write_tvar/2,retry/0,
             test/0]).

new_stick() -> new_tvar(true).

take_stick(S) -> 
  case read_tvar(S) of
    true  -> write_tvar(S,false);
    false -> retry()
  end.

put_stick(S) -> write_tvar(S,true).

start() -> S1 = atomically(fun() -> new_stick() end),
           S2 = atomically(fun() -> new_stick() end),
           S3 = atomically(fun() -> new_stick() end),
           S4 = atomically(fun() -> new_stick() end),
           S5 = atomically(fun() -> new_stick() end),
           spawn(fun() -> phil(1,S1,S2) end),
           spawn(fun() -> phil(2,S2,S3) end),
           spawn(fun() -> phil(3,S3,S4) end),
           spawn(fun() -> phil(4,S4,S5) end),
           %base:getLine(),
           phil(5,S5,S1).

phil(N,SL,SR) ->
  base:printLn(base:show(N) ++ " is thinking"), 
  %atomically (fun () -> take_stick(SL) end),
  %atomically (fun () -> take_stick(SR) end),
  atomically (fun () -> take_stick(SL),
                        take_stick(SR) end),
  base:printLn(base:show(N) ++ " is eating"), 
  atomically (fun() -> put_stick(SL) end),
  atomically (fun() -> put_stick(SR) end),
  phil(N,SL,SR).
