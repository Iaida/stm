#pragma once
#include "TVar.h"

#include <map>
#include <iostream>
#include <exception>
#include <set>


class transaction_exception_rollback : public std::exception
{
	virtual const char* what() const throw()
	{
		return "rollback";
	}
};

class transaction_exception_retry : public std::exception
{
	virtual const char* what() const throw()
	{
		return "retry";
	}
};


template <typename T>
class Transaction
{
public:
	T ReadTVar(TVar<T> & t_var);
	void WriteTVar(TVar<T> & t_var, T value);
	void Retry();
	bool ValidateReadSet();

	std::map<TVar<T> *, unsigned> read_set_; // saves version
	std::map<TVar<T> *, T> write_set_; // saves value
};

template<typename T>
inline T Transaction<T>::ReadTVar(TVar<T>& t_var)
{
	std::cout << "ReadTVar: " << t_var << "\n";

	// case lookup(Tvar,WS) of

	typename std::map<TVar<T> *, T>::iterator ws_it = write_set_.find(&t_var);
	//auto ws_it = write_set_.find(&t_var);
	
	/*none      -> {V, Ver} = core_read(Tvar),
					RS = get(rs),
					case lookup(Tvar, RS) of
						none->put(rs, insert(Tvar, Ver, RS));
						{value, Ver}->ok;
						{value, _  } -> throw(stm_exeption_rollback)
					end,
					V;*/

	if (ws_it == write_set_.end())
	{
		// not in map

		std::pair<T, unsigned> value_version_pair = t_var.read();
		
		typename std::map<TVar<T> *, unsigned>::iterator rs_it = read_set_.find(&t_var);
		//auto rs_it = read_set_.find(&t_var);
		
		if (rs_it == read_set_.end())
		{
			// not in map
			read_set_.emplace(&t_var, value_version_pair.second);
		}
		else if (rs_it->second == value_version_pair.second)
		{
			// same version number -> ok
		}
		else
		{
			// not same version number -> rollback
			throw transaction_exception_rollback();
		}
		return value_version_pair.first;
	}

	// {value,V} -> V

	else
	{
		// in map
		// return 2nd element of key-value-pair
		return ws_it->second;
	}

}

//write_tvar(Tvar, V) ->
//	%base:printLn({ write_tvar,Tvar }),
//	WS = get(ws),
//	put(ws, enter(Tvar, V, WS)).

template<typename T>
inline void Transaction<T>::WriteTVar(TVar<T>& t_var, T value)
{
	std::cout << "WriteTVar: " << t_var << " with value " << value << "\n";
	write_set_.emplace(&t_var, value);
}

//validate([]) -> true;
//validate([{Tvar,Ver}|RL]) ->
//  case core_read(Tvar) of
//    {_,Ver} -> validate(RL);
//    _       -> false
//  end.

template<typename T>
inline bool Transaction<T>::ValidateReadSet()
{
	for (auto & t_var_version_pair : read_set_)
	{
		// compare version numbers
		if (t_var_version_pair.first->read().second != t_var_version_pair.second)
			return false;
	}
	// reached end without different version numbers
	return true;
}

// retry() -> throw(stm_exception_retry).

template<typename T>
inline void Transaction<T>::Retry()
{
	throw transaction_exception_retry();
}


template<typename T>
void Atomically(std::function<void(Transaction<T> &)> f)
{
	// use while-loop with break on success to avoid stackoverflow with recursive calls

	while (true)
	{
		/*put(ws, empty()),
		put(rs, empty()),
		Me = self(),*/

		Transaction<T> t;

		//case catch Trans() of
		// do transaction / function
		try
		{
			f(t);

			// no exception thrown if this point was reached

			/*RS = get(rs),
			RL = to_list(RS),
			WS = get(ws),
			WL = to_list(WS),
			RWL = lists:umerge(keys(RS), keys(WS)),*/

			// get all tvars in read and write set
			std::set<TVar<T> *> read_write_t_vars;
			for (auto r : t.read_set_)
				read_write_t_vars.emplace(r.first);
			for (auto w : t.write_set_)
				read_write_t_vars.emplace(w.first);

			//%base:printLn({ lock_tvars,RWL }),
			//lists : map(fun(Tvar)->lock(Tvar) end, RWL),
			std::cout << "lock tvars \n";
			for (auto tvar : read_write_t_vars)
				tvar->lock();

			//%base:printLn(validate_tvars),
			std::cout << "validate tvars \n";

			//case validate(RL) of
			bool valid = t.ValidateReadSet();

			/*false -> % invalid = > rollback
						base : printLn(rollback),
						lists : map(fun(Tvar)->unlock(Tvar) 
									end, RWL),
						atomically(Trans);*/
			if (!valid)
			{
				std::cout << "rollback \n";
				// unlock tvars
				std::cout << "unlock tvars \n";
				for (auto tvar : read_write_t_vars)
					tvar->unlock();

				// repeat (recursive call)
				//Atomically<T>(f);
			}

			/*true  -> % valid = > commit
					%base:printLn(commit),
					lists : map(fun({ Tvar,V })->core_write(Tvar, V)
								end, WL),
					lists : map(fun(Tvar)->unlock(Tvar)
								end, RWL),
					Res*/
			else
			{
				std::cout << "commit \n";
				// write on write_set_
				for (auto w : t.write_set_)
					// write value of (tvar,value)-pair into tvar
					w.first->write(w.second);

				// unlock tvars
				std::cout << "unlock tvars \n";
				for (auto tvar : read_write_t_vars)
					tvar->unlock();

				// success
				break;
			}
		}
		//stm_exception_rollback ->...
		catch (const transaction_exception_rollback &)
		{
			std::cout << "transaction_exception_rollback \n";
			//Atomically<T>(f);
		}
		// stm_exception_retry ->...
		catch (const transaction_exception_retry &)
		{
			// handles same as rollback for now
			std::cout << "transaction_exception_retry \n";
			//Atomically<T>(f);
		}
	}

}
